import { Component, OnInit } from '@angular/core';
import { PlayersService } from '../services/players.service';
import { Player } from '../model/players'
@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  players: Player[];

  
  
  constructor(private playerService: PlayersService) { }

  ngOnInit() {
  }

  getPlayers(){
    this.playerService.getPlayers().subscribe(
      (res: Player[]  ) => {
        this.players = res;
      },
      err => console.error(err)
    );
  }

}
