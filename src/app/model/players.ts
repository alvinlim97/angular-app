export interface Player {
    // id?: number;
    // name: string;
    // height: number;
    // weight: number;
    // team: string;

    name: string;
    team_acronym: string;
    team_name: string;
    games_played: number;
    img: string;
  }