export interface Customer {
    // id?: number;
    // name: string;
    // height: number;
    // weight: number;
    // team: string;

    _id?: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: number;
    address: string;
    createdAt: Date;
  }