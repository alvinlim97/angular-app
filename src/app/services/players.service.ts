import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  API_URL = 'https://nba-players.herokuapp.com';
  constructor(private http: HttpClient) { }

  getPlayers() {
    return this.http.get(`${this.API_URL}/players-stats`);
  }

  
}
