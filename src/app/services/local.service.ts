import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from '../model/customers';

@Injectable({
  providedIn: 'root'
})
export class LocalService {


   localAPI = 'https://warm-springs-44843.herokuapp.com/api';
  constructor(private http: HttpClient) { }

  getLocal() {
    return this.http.get(`${this.localAPI}/customer`);
  }

  getCustomer(id: string) {
    return this.http.get(`${this.localAPI}/customer/${id}`);
  }

  saveCustomer(customer: Customer) {
    return this.http.post(`${this.localAPI}/customer`, customer);
  }

  updateCustomer(id: string | number, updateCustomer: Customer) {
    return this.http.put(`${this.localAPI}/customer/update?customerID=${id}`, updateCustomer);
  }

  deleteCustomer(id: string) {
    return this.http.delete(`${this.localAPI}/customer/delete?customerID=${id}`);
  }

  filterCustomerName(name: string) {
    return this.http.get(`${this.localAPI}/customer/get-filter-customer-by-name?name=${name}`);
  }
}
