import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayerComponent } from './player/player.component';
import { LocalComponent } from './local/local.component';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { CustomerComponent } from './customer/customer.component';
import { DataTableComponent } from './data-table/data-table.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: 'user',
    component: CustomerComponent,
    children: [
      { path: 'customers', component: LocalComponent, },
      { path: 'create', component: CustomerFormComponent },
      { path: 'edit/:id', component: CustomerFormComponent }
    ]
  },
  {
    path: 'players',
    component: PlayerComponent
  },
  {
    path: 'data-table',
    component: DataTableComponent
  },
  // {
  //   path: 'customers',
  //   component: LocalComponent,
  //   children: [
  //     { path: 'create', component: CustomerFormComponent }
  //   ]
  // }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
