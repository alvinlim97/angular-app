import { Component, OnInit } from '@angular/core';
import { Customer } from '../model/customers';
import { LocalService } from '../services/local.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  customer: Customer = {
    _id: '',
    firstName: '',
    lastName: '',
    email: '',
    phone: 0,
    address:'',
    createdAt: new Date()
  };


  edit = false;
  constructor(private localService: LocalService,
    private activeRoute: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    const params = this.activeRoute.snapshot.params;
    if (params.id) {
      this.localService.getCustomer(params.id).subscribe(
        (res: any) => {
          this.customer = res.result;
          this.edit = true;
        },
        err => console.error(err)
      );
    }
  }

  saveCustomer() {
    delete this.customer.createdAt;
    delete this.customer._id;
    this.localService.saveCustomer(this.customer).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/user/customers']);
      },
      err => console.error(err)
    );
  }

  updateCustomer() {
    delete this.customer.createdAt;
    this.localService.updateCustomer(this.customer._id, this.customer).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/user/customers']);
      },
      err => console.error(err)
    );
  }
}
