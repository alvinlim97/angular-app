import { Component, OnInit } from '@angular/core';
import { LocalService } from '../services/local.service';
import { Customer } from '../model/customers'

@Component({
  selector: 'app-local',
  templateUrl: './local.component.html',
  styleUrls: ['./local.component.css']
})


export class LocalComponent implements OnInit {


  cusName = '';

  searchTerm: Customer = {
    _id: '',
    firstName: '',
    lastName: '',
    email: '',
    phone: 0,
    address: '',
    createdAt: new Date()
  };
  customers: Customer[];
  constructor(private localService: LocalService) { }

  ngOnInit() {
    this.getLocals();
  }
  getLocals(){
    this.localService.getLocal().subscribe(
      (res: any  ) => {
        this.customers = res.result;
      },
      err => console.error(err)
    );
  }

  deleteCustomer(id: string) {
    this.localService.deleteCustomer(id).subscribe(
      res => {
        console.log(res);
        this.getLocals();
      },
      err => console.error(err)
    );
  }
  getCustomerByName() {
    console.log(this.cusName);
    if (this.cusName === '') {
      this.getLocals();
    }
     else {
      this.localService.filterCustomerName(this.cusName).subscribe(
        (res: any) => {
          this.customers = res.result;
        },
        err => (this.customers = [])
      );
    }
  }

}