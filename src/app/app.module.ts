import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlayerComponent } from './player/player.component';
import { PlayersService } from './services/players.service';
import { HttpClientModule } from '@angular/common/http';
import { LocalComponent } from './local/local.component';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { LocalService } from './services/local.service';
import { CustomerComponent } from './customer/customer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, 
  MatSortModule, MatTableModule } from "@angular/material";
import { DataTableComponent } from './data-table/data-table.component';
@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    LocalComponent,
    CustomerFormComponent,
    CustomerComponent,
    DataTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule
  ],
  providers: [
    PlayersService,
    LocalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
